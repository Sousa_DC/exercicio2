package exercicio2;

import java.lang.Math;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import java.util.Scanner;

public class Exercicio2 {
          
    public static void main(String[] args) {
        // TODO code application logic here
        int n, i;
        float soma = 0;
        
        Scanner ler = new Scanner(System.in);
           
        System.out.println("Introduza um numero inteiro : ");
        n = ler.nextInt();
        
        while (n>100){
            System.out.println("Introduza um numero inteiro : ");
            n = ler.nextInt();
        }

        for(i=0;i<=n;i++){
            if(i % 3 == 0){
                soma = soma + i;
            }              		
        }               
        System.out.println("A soma é: " + soma);  
    }
    
    
}

